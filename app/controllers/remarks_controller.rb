class RemarksController < ApplicationController
  def create
    photo_id = params[:id]
    remark = Remark.new(remark_params)
    remark.photo_id = photo_id
    remark.user_id = current_user.id
    p remark
    if remark.save
      redirect_to photo_path(photo_id)
    else
      redirect_to photo_path(photo_id)
    end
  end
  private

  def remark_params
    params.require(:remark).permit(:text, :rating)
  end
end
