class UsersController < ApplicationController
  def index
    @user = current_user
    @photos = Photo.where("user_id = ?", @user.id)
  end
end
