class PhotosController < ApplicationController
  before_action :authenticate_user!, only: :create

  def index
    # @photos = Photo.all.order(created_at)
    @photos = Photo.all
  end

  def show
    @photo = Photo.find(params[:id])
    @remark = Remark.new
    @remarks = Remark.where("photo_id = ?", @photo.id)
    if @remarks.empty?
      @average_remarks = 0
    else
      total = 0
      @remarks.each do |remark|
        total += remark.rating
      end
      @average_remarks = total/@remarks.size
    end
  end

  def new
    @photo = Photo.new
  end

  def create
    @photo = Photo.new(photo_params)
    @photo.user_id = current_user.id
    if @photo.save
      redirect_to root_url
    else
      render 'new'
    end
  end


  private

  def photo_params
    params.require(:photo).permit(:title, :image)
  end



end
