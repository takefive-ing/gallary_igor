class Remark < ActiveRecord::Base
  belongs_to :user
  belongs_to :photo

  validates :text,
            presence: true,
            length: {maximum: 250}

  validates :rating,
            presence: true,
            numericality: {
                :only_integer => true,
                :allow_nil => true,
                greater_than_or_equal_to: 1,
                less_than_or_equal_to: 5
            }
end
