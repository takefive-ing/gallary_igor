# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

users = []
5.times do
  name = Faker::Name.name
  users.push User.create(name: name, email: Faker::Internet.email(name), password: '12345678', password_confirmation: '12345678')
end

def photo_save(url_logo, title)
  dir = Rails.root.join('public', 'images')
  Dir.mkdir(dir) unless File.directory?(dir)
  require 'open-uri'
  new_file_path = Rails.root.join(dir, "#{title}.png")
  open(new_file_path, 'wb') do |file|
    file.write open(url_logo).read
  end
end

def create_photo(users)
  title = Faker::Company.name
  new_name = title.tr(' ', '_')
  photo_save(Faker::Company.logo, new_name)
  Photo.create(
      title: title,
      image: File.new( Rails.root.join('public', 'images',"#{new_name}.png")),
      user:users.sample
  )
end

photos = []
10.times do
    photos.push create_photo(users)
end

remarks = []

50.times do
  remarks.push Remark.create(
                   rating: rand(1..5),
                   text: Faker::Lorem.paragraph,
                   user: users.sample,
                   photo: photos.sample
               )
end
